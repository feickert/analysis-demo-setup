import optparse
import os
import tempfile
import ROOT
import shutil
from AnaAlgorithm.DualUseConfig import createAlgorithm

def setup_inputs(options):
    dataType = options.data_type
    sh = ROOT.SH.SampleHandler()
    sh.setMetaString( 'nc_tree', 'CollectionTree' )
    sample = ROOT.SH.SampleLocal (dataType)
    if dataType == "data" :
        sample.add (os.getenv ('ASG_TEST_FILE_DATA'))
        pass
    elif dataType == "mc" :
        sample.add (os.getenv ('ASG_TEST_FILE_MC'))
        pass
    elif dataType == "afii" :
        sample.add (os.getenv ('ASG_TEST_FILE_MC_AFII'))
        pass
    else:
        raise RuntimeError(f'invalid data type: {dataType}')
    sh.add (sample)
    sh.printContent()
    return sh


def make_job(options,sh):
    # Create an EventLoop job.
    job = ROOT.EL.Job()
    job.sampleHandler( sh )
    job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )


    from MuonAnalysisAlgorithms.MuonAnalysisAlgorithmsTest import makeSequence
    algSeq = makeSequence (options.data_type)
    for alg in algSeq:
        job.algsAdd( alg )
        pass


    alg = createAlgorithm('UserAlgorithm','UserAlgorithm')
    job.algsAdd(alg)

    # Make sure that both the ntuple and the xAOD dumper have a stream to write to.
    job.outputAdd( ROOT.EL.OutputStream( 'ANALYSIS' ) )
    return job

def run_job(options,job):
    # Find the right output directory:
    submitDir = options.submission_dir
    if os.path.exists(submitDir):
        shutil.rmtree(submitDir)
    # Run the job using the direct driver.
    driver = ROOT.EL.DirectDriver()
    driver.submit( job, submitDir )
    return submitDir


def main(options):
    assert ROOT.xAOD.Init().isSuccess()
    ROOT.xAOD.TauJetContainer()

    sh  = setup_inputs(options)
    job = make_job(options,sh)
    submitdir = run_job(options,job)

if __name__ == "__main__":
    parser = optparse.OptionParser()
    parser.add_option( '-d', '--data-type', dest = 'data_type',
                        action = 'store', type = 'string', default = 'data',
                    help = 'Type of data to run over. Valid options are data, mc, afii' )
    parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                    action = 'store', type = 'string', default = 'submitDir',
                    help = 'Submission directory for EventLoop' )
    ( options, args ) = parser.parse_args()
    main(options)